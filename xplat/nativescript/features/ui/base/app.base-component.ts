import { Component } from '@angular/core';

// libs
import { BaseComponent } from '@company/core';
import { AppService } from '@company/nativescript/core';

export abstract class AppBaseComponent extends BaseComponent {
  constructor(protected appService: AppService) {
    super();
  }
}
