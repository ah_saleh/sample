import { NgModule } from '@angular/core';

// xplat
import { FooCoreModule } from '@company/web';

@NgModule({
  imports: [FooCoreModule]
})
export class CoreModule {}
