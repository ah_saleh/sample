import { Component } from '@angular/core';

import { BaseComponent } from '@company/core';

@Component({
  selector: 'foo-home',
  templateUrl: 'home.component.html'
})
export class HomeComponent extends BaseComponent {}
