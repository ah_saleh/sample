import { NgModule } from '@angular/core';

// xplat
import { UIModule } from '@company/web';

const MODULES = [UIModule];

@NgModule({
  imports: [...MODULES],
  exports: [...MODULES]
})
export class SharedModule {}
