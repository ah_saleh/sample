import { NgModule } from '@angular/core';

// libs
import { FooIonicCoreModule } from '@company/ionic';

@NgModule({
  imports: [FooIonicCoreModule]
})
export class CoreModule {}
