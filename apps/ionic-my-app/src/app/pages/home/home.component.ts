import { Component } from '@angular/core';
import { BaseComponent } from '@company/core';

@Component({
  selector: 'page-home',
  templateUrl: 'home.component.html'
})
export class HomeComponent extends BaseComponent {}
